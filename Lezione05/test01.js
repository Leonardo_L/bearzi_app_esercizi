var me = {
    nome:"Leonardo",
    cognome:"Loppi",
    "età":27,
    sportpreferiti: [
        "nuoto",
        "snowboard",
    ]
};

for (var i in me ) {
    console.log(i);
    console.log(me[i]);
}

// FUNZIONI //

function sum (a,b,callback){
    var risultato = a+b;
    if (typeof callback =="function") callback(risultato);
    return risultato;
}

function quad(valore) {
    console.log("il quadrato di "+valore+" è "+(valore*valore));
}

function double(valore){
    console.log ("il doppio del valore è "+(valore*2));
}

console.log(sum(5,6,quad));
console.log(sum(2,65,double));
console.log(sum(7,6));
console.log(sum(5,-5));

//

var stringa = "Buongiorno a tutti";
console.log(stringa.split("a"));
console.log(stringa.split(" "));
//console.log(stringa.split(""));

var numero = 53213.646;
//console.log(numero.toString().split(""))
numero = numero.toString();
console.log(numero.split(""));