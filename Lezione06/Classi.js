class Being {
    constructor() {
        console.log("I am a Being.")
    }
    sayHi() {
        console.log("Hi");
    }    
}

class Animal extends Being {
    constructor(age) {
        super();
        this.age = age;
        this.favorite_food ="";
        this.noise = "";

        /*
        this.noise = "";
        var _this =this;
        setTimeout(function() {
            _this.makeNoise();
        },1000);
        */
        //setTimeout(this.makeNoise,1000);
        setTimeout(()=>this.makeNoise(),1000);
        console.log("I am an Animal and i am "+this.age+" years old.");
    }
    run() {
        console.log("I am running");
    }
    eat() {
        console.log("I eat "+this.favorite_food);
    }   
    makeNoise() {
        console.log(this.noise);
    }
}
class Dog extends Animal {
    constructor(age) {
        super(age);
        this.favorite_food = "Meat";
        this.noise ="Bau!";
        this._noiseInterval = setInterval(()=>this.makeNoise(),2000);
        console.log("I am a Dog and i am "+this.age+" years old.");
    }
    shhht() {
        clearInterval(this._noiseInterval);
        console.log("SHHHT!");
    }
}

class Cat extends Animal {
    constructor(age) {
        super(age);
        this.favorite_food = "Fish";
        this.noise ="Miao!";
        console.log("I am a Cat and i am "+this.age+" years old.");
    }
    run() {
        super.run();
        console.log("I run like a cat");
    }
}
var b = new Being();
b.sayHi();

console.log("--------------");

var a = new Animal(5);

console.log("--------------");

var d = new Dog(2);
d.run();
d.eat();
//d.makeNoise();

console.log("--------------");

var c = new Cat(3);
c.run();
c.eat();
//c.makeNoise();