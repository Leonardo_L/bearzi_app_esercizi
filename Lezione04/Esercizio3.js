/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : Esercizio3.js
*******************************************/
var me = {
 nome:"Leonardo",
 cognome: "Loppi",
 eta: 27,
 "sport preferiti":[
     "Nuoto",
     "Snowboard",
 ]
}

console.log(me);
console.log("nome "+me.nome);
console.log("nome "+me ["nome"]);
console.log("sport preferiti "+me["sport preferiti"]);
me.sesso = "maschile"
console.log(me);

if (me.eta>20) {
    console.log(me.nome+" ha più di 20 anni");
}else{
    console.log(me.nome+" ha meno di 20 anni");

}

if (me.sesso!=="maschile") console.log(me.nome+" non è un uomo");
if (me.sesso==="maschile") console.log(me.nome+" è un uomo");

if (typeof me2 !== "undefined") {
    console.log ("me2 è definita",me2);
}

if (typeof me !== "undefined") {
    console.log ("me è definita",me);
}