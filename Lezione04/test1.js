/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : test1.js
 *******************************************/

 /* stampo nome e cognome */
 var nome;
 nome="Leonardo";

 var cognome="Loppi";
 var eta=27;

 console.log("Esercizio Javascript 1");
 console.log(nome)
 console.log(nome+" "+cognome+" "+eta);
 console.log("tra dieci anni: "+(eta+10));
 console.log("eta / 2 = "+(eta/2));
 console.log("eta * 2 = "+(eta*2));
 console.log("eta % 2 = "+(eta%2));
 console.log(eta);
 console.log("eta ++ = "+(eta++));
 console.log(eta);
 console.log("-------");
 console.log("eta ++ = "+(++eta));
 console.log(eta);
 console.log("eta -- = "+(--eta));
 console.log("eta -= 20 = "+(eta-=20));
 console.log("-------");
 console.log("true= "+(true));
 console.log("!true= "+(!true));
 console.log("false= "+(false));
 console.log("!false= "+(!false));
 console.log("1=2:"+(1==2));
 console.log("!1=2:"+(!(1==2)));
 console.log("1!=2:"+(1!=2));
 console.log("1==\"1\":"+(1=="1"));
 console.log("1===\"1\":"+(1==="1"));
 console.log("1!==\"1\":"+(1!="1"));
 console.log("1!===\"1\":"+(1!=="1"));
